const path = require('path')

module.exports = {
    mode: 'development',
    devServer: {
        contentBase: path.join(__dirname, './app/static/dist'),
        compress: true,
        publicPath: '',
        open: true,
        hot: true,
        port: 9000,
        proxy: {
            '!(/static/dist/**/**.*)': {
                target: 'http://localhost:5000',
            },
        },
    },
    entry: './app/static/src/index.js',
    output: {
        path: path.resolve(__dirname, 'app/static/dist'),
        filename: 'bundle.js'
    },
    module: {
        rules: [
            {
                test: /\.js$/,
                exclude: /(nodes_modules)/,
                use: {
                    loader: 'babel-loader',
                    options: {
                        presets: ['@babel/preset-env']
                    }
                }
            },
            {
                test: /\.(png|jpe?g|gif|svg)$/,
                use: [
                    {
                        loader: 'file-loader',
                        options: {
                            outputPath: 'assets'
                        }
                    }
                ]
            }
        ],
    },
}
