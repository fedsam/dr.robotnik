export class Board {
    reset() {
        this.grid = this.getEmptyBoard();
    }

    getEmptyBoard() {
        return Array.from({ length: 20 }, () => Array(9).fill(null));
    }

    /** À CORRIGER 
    clearGrid() {
        this.grid = this.grid[0].map((_, colIndex) => this.grid.map(row => row[colIndex]));
        for (let i = this.grid.length - 1; i > 0; --i) {
            let tmpArray = this.grid[i].filter(el => el != null);
            this.grid[i] = [...Array(this.grid[0].length - tmpArray.length).fill(null), ...tmpArray];
        }
        this.grid = this.grid[0].map((_, colIndex) => this.grid.map(row => row[colIndex]));
        for (let i = this.grid.length - 1; i > 0; --i) {
            for (let j = 0; j < this.grid[0].length; ++j) {
                if (this.grid[i][j] != null) {
                    this.grid[i][j].x = j * 30;
                    this.grid[i][j].y = i * 30;
                } 
            }
        }
    }
    **/

}
